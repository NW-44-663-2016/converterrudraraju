﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterRudraraju.Models;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ConverterRudraraju.Controllers
{
    public class ConverterController : Controller
    {
        public IActionResult Rudraraju()
        {
            ViewData["Title"] = "ConverterApp By Sandeep";
            ViewData["Protip"] = "Sandeep";
            ViewData["Value"] = "";

            Converter converter = new Converter();
            return View(converter);
         }

        
        public IActionResult Convert(Converter converter)
        {
            if (ModelState.IsValid)
            {
                int temp = (int)((converter.Temperature_F - 32) * 5.0 / 9.0);
                ViewData["Title"] = "Converted by Rudraraju";
                ViewData["Result"] = "Temperature in C = " + temp;
                ViewData["Value"] = temp;
            }
            return View("Rudraraju", converter);
        }
    }
}
